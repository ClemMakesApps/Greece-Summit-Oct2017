# GitLab Greece Summit 2017

### Who:

All GitLab team, and core team

### What:

We try to get together every 9 months or so to all get some face time together,
build community, and get some work done!

### Where:

[Crete, Greece](https://en.wikipedia.org/wiki/Crete)

### When: 

- Arrival to the hotel on October 19, 2017 (this could mean leaving your home a day before)
- Departure from the hotel on October 25, 2017

Note: Regular check-in/check-out times apply.

### How: General overview of the schedule (calendar invites + details to follow)

- Thursday - arrival day w// airport transfer arranged
- Friday - Sid welcome keynote + amazing race team bonding game
- Saturday - Day trip to Santorini!
- Sunday - afternoon tour of Knossos Palace + Archaeological museum in Heraklion
- Monday - work day w/ [UGC sessions](https://docs.google.com/spreadsheets/d/15irgt0kOlCNzltsBzWRbJoQzLFjOlOYKgREQ_PRGiuQ/edit#gid=0) & a game night in the evening
- Tuesday - work day w/ [UGC sessions](https://docs.google.com/spreadsheets/d/15irgt0kOlCNzltsBzWRbJoQzLFjOlOYKgREQ_PRGiuQ/edit#gid=0) & the Final party (surprise theme, 200 costumes provided)
- Wednesday - departure day w/ airport transfer arranged

### General info

Check the closed issues with for questions that have been asked and if yours hasn't been answered, please open a new issue.

Please read our [Summit page](https://about.gitlab.com/culture/summits) with the general information provided **carefully**, especially the [General Info](https://about.gitlab.com/culture/summits/#general-info) section regarding bringing kids and SOs.
- Please understand that **everyone will share a room** with a team member with the exeption of a specific (medical) reason.<br>
- Private costs for a private room; **~ €140 (not expensable)** <br>
- Private hotel costs for bringing your SO; **~ €222 (not expensable)**, plus any other costs involved such as flight or visa. This will cover all food and beverage at the hotel as well (not just the room).  <br>
- NOTE All additional costs will be paid to GitLab upon checkin. Please bring cash in EUROs. 
- SO's are welcome to all activites/excursions free of charge. 

### Bringing kids

- Please read [our policy on bringing kids to the summits on our Summit page](https://about.gitlab.com/culture/summits/#having-your-children-join-you). <br>
- Private hotel costs for an extra room for your kids 0-12 years old; **~ €245 (not expensable)**, plus any other costs involved such as flight or visa. This includes all food and beverage at the hotel as well. <br>
- Private costs for an extra room for your kids 12-18 years old; **~ €330 (not expensable)**, plus any other costs involved such as flight or visa. This includes all food and beverage at the hotel as well. <br>
- NOTE Since the rooms have 2 **twin** beds, there can only be 2 people per room, kids or adults. Please keep this in mind when making your decision to have your SO/kids join us.

Any additional nights at the hotel will not be eligible for the group rate, but instead will be at full hotel pricing. Please book these accommodations separately with the hotel. 

### List of important documents/resources 

- [Important Flights Info](https://gitlab.com/summits/Greece-Summit-Oct2017/blob/master/Important-flight-info.md)
- [Flight & Visa & Rooms info](https://docs.google.com/a/gitlab.com/spreadsheets/d/1SO9hpyt5mFmCV2negTicYum4iT7ebUlcGvszSnEX7UY/edit?usp=sharing) commentable with GitLab login + for core team
- [Egencia](https://www.egencia.com/) can be used to book your flight (no cc needed) or feel free to use any other means and expense your flight (Expensify for employees / your monthly bill for contractors)
- [Hotel pictures & general info](https://www.iberostar.com/en/hotels/creta/iberostar-creta-panorama-mare)
- [Summit FAQ]
- [Packing List] 
- [Expenses] 
- [Getting Work Done]
- [Board Game List](https://docs.google.com/a/gitlab.com/spreadsheets/d/1MH1wHT4jBKOCpup7IuliVRKn6dJtPDp6A17syc5adJo/edit?usp=sharing)

### External

- [CDC Health information](https://wwwnc.cdc.gov/travel/destinations/traveler/children.pregnant.vfr/greece?s_cid=ncezid-dgmq-travel-single-001)
- [Visa info](http://www.mfa.gr/en/visas/visas-for-foreigners-traveling-to-greece/countries-requiring-or-not-requiring-visa.html) > get in touch with Brittany if you need a visa, assistance with obtaining a passport, or assistance with a passport renewal.
    * If you are an EU national, you can travel with your [national ID card or passport](http://europa.eu/youreurope/citizens/travel/entry-exit/eu-citizen/index_en.htm) when you are traveling from one border-free Schengen EU country to another.
    * Please set your visa appointment no earlier than 2017-07-21, but please make it as close to this date as possible in case we need to make any additional appointments. You can make an appointment by visiting the [embassy in your country](http://www.mfa.gr/missionsabroad/en/). You can also view the necessary documents on this site. 
    * For more information on how to apply for a visa, and what information is needed please go to the [visa page in our handbook](https://about.gitlab.com/handbook/people-operations/visas/). 

### Booking flights

Please research flight options carefully, especially ones with layovers! Some connections require you to change airports, try to avoid that. 

- [Egencia](https://www.egencia.com/) can be used to book your flight (no cc needed) or feel free to use any other means and expense your flight (Expensify for employees / your monthly bill for contractors)
- When expensing in Expensify please categorize as "Other" and then choose "Greece Summit 2017" as a tag.

### Questions? 

+ Create an [issue](https://gitlab.com/summits/greece-summit-oct2017/issues/new) and cc both @kirstenabma and @brittanyr