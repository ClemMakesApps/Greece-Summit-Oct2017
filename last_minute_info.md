We're getting close to our trip to Crete, Greece, yay! Since we've got 2 more weeks, here's some very important intel!
This is mandatory reading to make sure you take maximum advantage of this awesome week of GitLab fun. Feel free to forward this to your SO.
Check your calendar for the week of October 19th - 25th and make sure to ask your questions before traveling to Crete. 
This can be either in the #fall17-summit channel on Slack or directly over email to Kirsten
Understand that all activities are optional, but make sure to RSVP yes/no ASAP (Before Monday 16th). When responding with “maybe” it will be recorded as a "no".

Attached are the schedules for the arrivals transfer groups, for the UGC sessions with the topics and moderator,  and the final rooming list
Please check these carefully! If something of the hotel rooms is off, please mention the line/room number with your message.

# SO’s and Single Room charges

Single room costs are EUR140
SO room costs are EUR 222
These fees are due upon arrival in cash and before checking in
No fees paid means no hotel room key!
We can provide you with a receipt if preferred
Arriving in Heraklion, Greece

Arrival before Thursday: when arriving at HER please take some time and see if you can combine traveling to the hotel with other team members arriving. We will not provide transfers for arrivals before the 19th, nor have we booked a hotel room unless we've been in touch about this before today. Taxies are at your own expense (excluding the e-team that’s arriving early)

If you are arriving Thursday at any time we've arranged a transfer schedule that is set for these specific number of arrivals. Make sure to move through the airport and luggage retrieval quickly and meet our partner outside the arrivals hall. They can be recognized by the GitLab sign.
NOTE: Try to contact Kirsten (see phone # below or use kirsten@gitlab.com) asap (when possible) if you are delayed so we can make sure you can join another transfer bus.

When you arrive at the hotel, check in at the GitLab desk in the hotel lobby for some custom made GitLab Greece swag and hotel info before checking in at the front desk.
# Address
Iberostar Panorama & Mare
Address: Panorama, 74100, 741, Ag. Rafail, Vathi 283 00 (Rethymno,Crete, Greece)
Phone: +34 971 07 60 00

# Phone number

Save at least Kirsten’s phone number in your phone in case your flight is delayed, you get kidnapped our get in any other trouble. And no, we don’t have the money to bail you out of jail.
Kirsten +1-415-966-8495

# Packing list

The weather is expected to be around 70F/21C degrees. See a small packing list as an example of items to bring on the project page. Feel free to suggest other items in the #fall17-summit channel on Slack, or as a merge request for that page.
GitLab swag & representation

In Greece we’re handing out a lot of awesome, custom made swag and of course we want to wear it and take it home! Make sure to keep room in your suitcase. Keep in mind that once you put on something with the GitLab logo people will see you as a representative of our company, even at evening events or at the hotel bar. If you choose to drink, do so wisely.  Choosing to be irresponsible puts your future travel, reputation and possibly your career at risk. Good life rule, really. You don’t have to drink, go anywhere, or do anything you don’t want to do. You represent GitLab on this trip, and we party smart.

# WiFi at the Summit
We have limited Wifi bandwidth at the event. We’d like to ask you to refrain from streaming videos and downloading your tv shows just for this week so we can use our dedicated WiFi network for work stuff and sharing our summit with the community as smoothly and much as possible. Let’s show good sportsmanship and have the connectivity frustration stay at a minimum this week.

# Live streaming all the things!
We will have a live video crew on site the whole week, and live streaming all the things. Yes, everything that goes on this week will be on a special YouTube live stream. Having signed on to a fully transparent and open source company, you can expect that we’ll share pictures and video about our company and team. This also includes a live stream this summit. If you really do not wish to be on camera, please let us know as soon as possible. 
We will update you as soon as we have everything set for the live stream so you can share with friends, family and other that wanna watch our craziness this week.

# Getting work done
We’ve got a large meeting room arranged and blocked some time on the schedule to get some work done and keep up to date with everything. If your team would like to meet during the summit, please check the schedule when there’s time to meet. We’ve got 2 smaller meeting rooms and there’s a lot of room around the hotel to meet in smaller groups.
Significant Others

SOs are welcome to join the team during all activities. Please RSVP with +1 when your SO would like to join. Details about expenses covered or not is on the project page and summit page in the handbook. 

# Expenses
Since the hotel is all inclusive and pre-paid, just as all activities, any costs you make outside of those are at your own expense and cannot be sent in for reimbursements. Keep this in mind at the hotel bar too; domestic alcohol is covered but international brands are not. Only transportation to and from the airport can be expensed if approved before hand. More info on Expenses

# Activity info
RSVP to the calendar invites (before Monday 16th!) For all these activities you only have to show up. Please understand; with the group we have in Greece we’ll be very punctual.
If you’re late, we won’t wait!
Some tips to bring with your for activities:
Make sure to pack some warmer clothes, it can get chilly at night
Pack a bathing suit or outfit you don’t mind getting wet for swimming etc. Well.. we’re sure you figured out you’d need one.
Pack comfortable walking / hiking shoes for the Santorini trip and Heraklion trip.
Bring your laptop (duh)
Bring your own (homemade) Toga if you have one, but optional as we’ll provide the Toga Tuesday party with 200 costumes. There will be awards for “Best interpretation of the theme” so go Greek here :)
More packing tips are on the project page on the Packing List

# TL;DR

1. Pack wisely
1. RSVP
1. Get cash for your SO or single room costs if applicable
1. Check the arrivals schedule for your transfer group
1. Consult Slack channel for event updates and questions while there
1. Introduce yourself to new faces
1. Don’t forget sunscreen & sunglasses
1. Call Kirsten when you get kidnapped
1. Have fun!